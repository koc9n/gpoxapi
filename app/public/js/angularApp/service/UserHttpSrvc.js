/**
 * Created by koc9n on 21.02.16.
 */
(function () {
    'use strict';
    angular.module('AdminApp').service('UserHttpSrvc', ['$http', function ($http) {
        var API_BASE_URL = "/api/v0.0.1";

        this.getUsers = function () {
            return $http.get(API_BASE_URL + '/users');
        };
    }]);
}());
