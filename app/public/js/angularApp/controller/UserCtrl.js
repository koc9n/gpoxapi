/**
 * Created by koc9n on 21.02.16.
 */
(function () {

    'use strict';

    angular.module('AdminApp').controller('UserCtrl', ['$scope', '$location', 'ToasterSrvc', 'UserHttpSrvc', function ($scope, $location, ToasterSrvc, UserHttpSrvc) {

        $scope.users = [];

        UserHttpSrvc.getUsers().then(function (response) {
            $scope.users = response.data;
        }, function (reason) {
            console.log('error: ' + reason);
        });
    }]);
}());
