/**
 * Resource not found error
 */
module.exports.handle404 = function (req, res, next) {
    var err = new Error("Not Found");
    err.status = 404;
    sendErrorResponse(req, res, err);
};

/**
 * Internal error
 */
module.exports.handleInternalError = function (err, req, res, next) {
    console.log("||---- Internal Error ----||");
    console.log(err);

    if (!err.status) {
        err.status = 500;
    }

    sendErrorResponse(req, res, err)
};

function sendErrorResponse(req, res, err) {
    if (req.xhr) {
        res.status(err.status).json({
            error: {
                message: "Internal Error",
                description: 'See logs on server'
            }
        });
    } else {
        res.status(err.status).render("errors/" + err.status, {
            message: err.message,
            error: err.error || err // include stacktrace for Dev mode
        });
    }
}