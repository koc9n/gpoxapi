/**
 * Created by koc9n on 4/18/16.
 */

var FB = require('fb');
var env = process.env.NODE_ENV || "dev";
var config = require('../config/config.json')[env];

module.exports.getExtendedToken = function (facebookToken, resultHandler) {
    FB.api('oauth/access_token', {
        client_id: process.env.FB_APP_ID || config.fb.app_id,
        client_secret: process.env.FB_APP_SECRET || config.fb.app_secret,
        grant_type: 'fb_exchange_token',
        fb_exchange_token: facebookToken
    }, function (res) {
        resultHandler(res.error, res);
    });
};

/**
 * retrieve list of friends and friends of friends
 * @param facebookToken
 * @returns {*}
 */
module.exports.getFriends = function (facebookId, facebookToken, resultHandler) {
    FB.api(facebookId + '/friends', {
        access_token: facebookToken
    }, function (res) {
        resultHandler(res.error, res);
    });
};
