/**
 * Created by koc9n on 5/28/2016 AD.
 */

var userDao = require('./../dao/sequelize/UserDAO');
var relationsDAO = require('./../dao/sequelize/RelationsDAO');
var couponDAO = require('./../dao/sequelize/CouponDAO');
var Promise = require("bluebird");
var fbService = Promise.promisifyAll(require('./FaceBookService'));
var randToken = require('rand-token');
var moment = require('moment');
const OPTIONS = {
    MALE: {
        MY_OFFER: 2,
        ANYTHING_YOU_WANT: 1,
        NEXT: 0
    },
    FEMALE: {
        YES: 1,
        NO: 0
    }
};


module.exports.helper = require('./helper/helper');

module.exports.getAll = function () {
    return userDao.findAll();
};

module.exports.delete = function (id) {
    return userDao.delete(id)
        .then(function (user) {
            if (user) {
                return user;
            } else {
                throw new Error('can not find user by token');
            }
        });
};

module.exports.findByToken = function (token) {
    return userDao.findByToken(token)
        .then(function (user) {
            if (user) {
                return user;
            } else {
                throw new Error('can not find user by token');
            }
        })
};

module.exports.find = () => userDao.findById(id);

module.exports.findById = function (id) {
    return userDao.findById(id)
        .then(function (user) {
            if (user) {
                return user;
            } else {
                throw new Error('can not find user by id');
            }
        })
};

module.exports.findByTokenUnscoped = function (token) {
    return userDao.findByTokenUnscoped(token)
        .then(function (user) {
            if (user) {
                return user;
            } else {
                throw new Error('can not find user by token');
            }
        })
};

module.exports.findByIdUnscoped = function (token) {
    return userDao.findByFacebookIdUnscoped(token)
        .then(function (user) {
            if (user) {
                return user;
            } else {
                throw new Error('can not find user by id');
            }
        })
};

module.exports.updateById = function (id, userToUpdate) {
    return this.findById(id)
        .then(function (user) {
            if ((userToUpdate.CouponUuid || userToUpdate.Coupon)
                && user.gender == null && userToUpdate.gender == null) {
                throw new Error('user should have gender before choose coupon');
            } else if ((userToUpdate.CouponUuid || userToUpdate.Coupon) && user.gender === 1) { //if female
                throw new Error('ladies can not choose coupon');
            }

            if (userToUpdate.Coupon) {
                if (!user.coupon) {
                    return couponDAO.insert(userToUpdate.Coupon).then(function (coupon) {
                            user.setCoupon(coupon);
                            user.set(userToUpdate);
                            return user.save();
                        }
                    );
                }
            }

            user.set(userToUpdate);
            return user.save()
        })
        .then(function (user) {
            return user.reload();
        });
};

module.exports.authenticateFBUser = function (fbUser) {
    return userDao.findByFacebookId(fbUser.facebookId)
        .then(function (user) {
            if (process.env.NODE_ENV != 'prod') {
                return Promise.resolve(null);
            } else {
                return fbService.getExtendedTokenAsync(fbUser.facebookToken)
                    .bind(user)
            }
        })
        .then(function (data) {
            var secureData = {};
            if (process.env.NODE_ENV != 'prod') {
                secureData.token = fbUser.facebookToken;
                secureData.fbToken = fbUser.facebookToken;
            } else {
                secureData.token = randToken.generate(data.access_token, 64);
                secureData.fbToken = data.access_token;
            }
            if (user) { // if login from other device by example
                secureData.user = user.id;
                return secureData;
            } else {
                var user = {};
                user.facebookId = fbUser.facebookId;
                user.Config = {}; // create default config
                return userDao.create(user)
                    .then((user) => {
                        secureData.user = user.id;
                        return secureData;
                    })
            }
        })
};

module.exports.getConfig = function (id) {
    return this.findById(id)
        .get('Config')
};

module.exports.updateConfig = function (id, config) {
    return this.findById(id)
        .then(function (user) {
            var userConfig = user.getDataValue('Config');
            userConfig.set(config);
            return userConfig.save();
        })
        .then(function (config) {
            return config;
        })
};

module.exports.makeVote = function (id, vote) {
    return this.findById(id)
        .then(function (user) {
            return this.findById().bind({tokenUser: user});
        })
        .then(function (user) {
            var relation = {};
            if (isMale(this.tokenUser) && isFemale(user)) {
                relation.maleChoise = vote.option;
                relation.maleUserId = this.tokenUser.facebookId;
                relation.femaleUserId = vote.id;
                relation.maleChoosedAt = new Date();
            } else if (isFemale(this.tokenUser) && isMale(user)) {
                relation.femaleChoise = vote.option;
                relation.femaleUserId = this.tokenUser.facebookId;
                relation.maleUserId = vote.id;
                relation.femaleChoosedAt = new Date();
            } else {
                throw new Error("Can not be like between the same gender.");
            }
            return relationsDAO
                .findOrCreateRelationByUserIds(relation.maleUserId, relation.femaleUserId, relation)
                .bind({tokenUser: this.tokenUser});
        })
        .spread(function (relation, created) {
            if (created) {
                return relation;
            } else {
                if (isFemale(this.tokenUser)) {
                    if (relation.femaleChoise) {
                        throw new Error("Trying to vote second time!!! Enough!!!");
                    }
                    if (relation.maleChoise === OPTIONS.MALE.ANYTHING_YOU_WANT) {
                        throw new Error("He proposed *ANYTHING_YOU_WANT*. Deprecated to make vote in this version of app.");
                    }
                    relation.femaleChoise = vote.option;
                    relation.femaleChoosedAt = new Date();
                } else if (isMale(this.tokenUser)) {
                    if (relation.maleChoise) {
                        if (relation.maleChoise === OPTIONS.MALE.MY_OFFER
                            && relation.femaleChoise === OPTIONS.FEMALE.NO) {
                            if (moment.duration(moment().diff(moment(relation.femaleChoosedAt))).days() <= 30) {
                                throw new Error("You need to wait 30 days. You said *My OFFER* and she said *NO*");
                            } else if (vote.option === OPTIONS.MALE.MY_OFFER) {
                                throw new Error("Option *My OFFER* is not available. She said *NO*");
                            }
                        }
                        throw new Error("Trying to vote second time!!! Enough!!!");
                    }
                    if (relation.femaleChoise === OPTIONS.FEMALE.YES
                        && vote.option === OPTIONS.MALE.ANYTHING_YOU_WANT) {
                        throw new Error("Option *ANYTHING_YOU_WANT* is not available. She said you *YES*");
                    }
                    if (relation.femaleChoise === OPTIONS.FEMALE.NO
                        && vote.option === OPTIONS.MALE.MY_OFFER) {
                        throw new Error("Option *MY_OFFER* is not available. She said you *NO*");
                    }
                    relation.maleChoise = vote.option;
                    relation.maleChoosedAt = new Date();
                }
                return relation.save();
            }
        })
        .then(function (relation) {
            return relation;
        })
};

function isMale(user) {
    return user.gender === 0;
}

function isFemale(user) {
    return user.gender === 1;
}

