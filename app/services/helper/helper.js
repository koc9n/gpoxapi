/**
 * Created by koc9n on 5/28/2016 AD.
 */

module.exports.getMobileUser = function (result) {
    return {
        token: result.token,
        facebookId: result.facebookId,
        facebookToken: result.facebookToken
    };
};

module.exports.catchError = function (err, next) {
    console.log(err);
    var error = new Error("Internal Error");
    error.message = err.message;
    error.error = err.error || err;
    next(error);
};