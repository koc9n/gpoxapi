/**
 * Created by koc9n on 5/20/2016 AD.
 */

"use strict";

module.exports = function (sequelize, DataTypes) {
    var Config = sequelize.define("Config", {
        distance: {
            type: DataTypes.INTEGER(3),
            allowNull: false,
            defaultValue: 15,
            validate: {min: 1, max: 100}
        },
        minAge: {
            type: DataTypes.INTEGER(2),
            allowNull: false,
            defaultValue: 18,
            validate: {min: 18, max: 81}
        },
        maxAge: {
            type: DataTypes.INTEGER(2),
            allowNull: false,
            defaultValue: 81,
            validate: {min: 18, max: 81}
        }
    });

    return Config;
};