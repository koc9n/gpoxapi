/**
 * Created by koc9n on 4/16/16.
 */

"use strict";

module.exports = function (sequelize, DataTypes) {
    var Coupon = sequelize.define("Coupon", {
        uuid: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            primaryKey: true
        },
        couponId: {
            type: DataTypes.STRING,
            allowNull: false
        },
        dealUrl: {
            type: DataTypes.STRING,
            allowNull: false
        },
        title: {
            type: DataTypes.STRING
        },
        largeImageUrl: {
            type: DataTypes.STRING
        },
        state: {
            type: DataTypes.INTEGER
        }
    }, {
        classMethods: {
            associate: function (models) {
                Coupon.hasMany(models.User);
            }
        }
    });

    return Coupon;
};