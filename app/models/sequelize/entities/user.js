/**
 * Created by koc9n on 4/14/16.
 */

"use strict";

module.exports = function (sequelize, DataTypes) {

    var User = sequelize.define("User", {
        facebookId: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
            //primaryKey: true
        },
/*        facebookToken: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        token: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },*/
        name: {
            type: DataTypes.STRING
        },
        lastName: {
            type: DataTypes.STRING
        },
        gender: {
            type: DataTypes.INTEGER(1),
            allowNull: true,
            validate: {len: [0, 1]}
        },
        birthday: {
            type: DataTypes.DATE
        },
        pictureUrl: {
            type: DataTypes.STRING
        },
        online: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        disableFAFOF: { // option to remove FAFOF from search (FAFOF - Friends And Friends Of Friends)
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        lat: DataTypes.FLOAT,
        lng: DataTypes.FLOAT
        //,
        //location: {
        //    type: DataTypes.GEOMETRY // TODO Check how to use with heroku
        //}
    }, {
        classMethods: {
            associate: function (models) {
                User.belongsTo(models.Config);
                User.belongsTo(models.Coupon);
                User.hasMany(models.Relations, {
                    foreignKey: 'maleUserId',
                    as: "maleRelations"
                });
                User.hasMany(models.Relations, {
                    foreignKey: 'femaleUserId',
                    as: "femaleRelations"
                });
                User.addScope('includeConfig', {
                    include: [
                        {model: models.Config}
                    ]

                });
                User.addScope('includeCoupon', {
                    include: [
                        {model: models.Coupon}
                    ]
                });
                User.addScope('maleRelations', {
                    include: [
                        {model: models.Relations, foreignKey: 'maleUserId', as: 'maleRelations'}
                    ]
                });
                User.addScope('femaleRelations', {
                    include: [
                        {
                            model: models.Relations,
                            foreignKey: 'femaleUserId',
                            as: 'femaleRelations'
                        }
                    ]
                });
            }
        },
        scopes: {
            isOnline: {
                where: {
                    online: true
                }
            },
            male: {
                where: {
                    gender: {$eq: 0}
                }
            },
            female: {
                where: {
                    gender: {$eq: 1}
                }
            }
        }
    });

    return User;
};





