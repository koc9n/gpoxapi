/**
 * Created by koc9n on 5/20/2016 AD.
 */
"use strict";

module.exports = function (sequelize, DataTypes) {
    var Relations = sequelize.define("Relations", {
        maleChoise: {
            type: DataTypes.INTEGER(1),
            allowNull: true,
            validation: {
                len: [0, 2]
            }
        },
        femaleChoise: {
            type: DataTypes.INTEGER(1),
            allowNull: true,
            validation: {
                len: [0, 1]
            }
        },
        maleChoosedAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        femaleChoosedAt: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        classMethods: {
            associate: function (models) {
                Relations.belongsTo(models.User, {model: models.User, foreignKey: 'maleUserId', as: 'maleUser'});
                Relations.belongsTo(models.User, {model: models.User, foreignKey: 'femaleUserId', as: 'femaleUser'})
            }
        }
    });

    return Relations;
};