/**
 * Created by koc9n on 4/15/16.
 */
"use strict";

var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");

var env = process.env.NODE_ENV || "dev";
var config = require(path.join(__dirname, '../..', 'config', 'config.json'))[env];
var dbUrl = process.env.DATABASE_URL || config.db.dbUrl;
var sequelize = new Sequelize(dbUrl, {dialectOptions: {ssl: config.db.ssl}});
var db = {};
var entitiesDir = path.join(__dirname, './entities');

fs
    .readdirSync(entitiesDir)
    .forEach(function (file) {
        var model = sequelize.import(path.join(entitiesDir, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(function (modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;