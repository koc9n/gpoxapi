/**
 * Created by koc9n on 4/16/16.
 */
var models = require('../../models/sequelize/index'),
    Coupon = models.Coupon;

module.exports.findAll = function () {
    return Coupon.findAll();
};

module.exports.findByCouponId = function (couponId) {
    return Coupon.findOne({where: {couponId: couponId}});
};

module.exports.insert = function (coupon) {
    return Coupon.create(coupon);
};

module.exports.delete = function (id) {
    return Coupon.destroy({where: {id: id}});
};

module.exports.addCoupons = function (coupons) {
    return models.sequelize.transaction(function (t) {
        coupons.forEach(function (key, val) {
            Coupon.save(val, {transaction: t})
                .catch(function (err) {
                    console.log("Err during saving Coupons: \n");
                    console.log(err);
                });
        });
    })
};