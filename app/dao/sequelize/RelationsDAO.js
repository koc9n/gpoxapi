/**
 * Created by koc9n on 6/1/2016 AD.
 */
var models = require('../../models/sequelize/index'),
    Relations = models.Relations;

module.exports.findAll = function () {
    return Relations.findAll();
};

module.exports.findOrCreateRelationByUserIds = function (maleUserId, femaleUserId, data) {
    return Relations.findOrCreate({
        where: {
            maleUserId: maleUserId,
            femaleUserId: femaleUserId
        },
        defaults: data
    });
};

module.exports.put = function (data) {
    return Relations.update(data);
};
