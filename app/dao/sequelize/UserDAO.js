/**
 * Created by koc9n on 4/9/16.
 */
var models = require('../../models/sequelize/index');
var User = models.User;

module.exports.findAll = function () {
    return User.findAll();
};

module.exports.findByToken = function (token) {
    return User.scope('includeCoupon', 'includeConfig', 'maleRelations', 'femaleRelations')
        .findOne({
            where: {
                token: token
            }
        });
};

module.exports.findByTokenUnscoped = function (token) {
    return User.findOne({
        where: {
            token: token
        }
    });
};

module.exports.find = function (condition, attributes, scopes) {
    return User.scope(scopes).findAll({
        where: condition,
        attributes: attributes
    });
};

module.exports.findById = function (id) {
    return User
        .scope('includeCoupon', 'includeConfig', 'maleRelations', 'femaleRelations')
        .findById(id);
};

module.exports.findByFacebookIdUnscoped = function (facebookId) {
    return User.findById(facebookId);
};

module.exports.findByFacebookId = function (facebookId) {
    return User.findOne({facebookId: facebookId});
};

module.exports.create = function (user) {
    var user = User.build(user, {include: models.Config});
    return user.save();
};

module.exports.update = function (user, condition) {
    return User.update(user, {where: condition, include: [models.Config, models.Coupon]});
};

module.exports.delete = function (id) {
    return User.destroy({where: {id: id}});
};


