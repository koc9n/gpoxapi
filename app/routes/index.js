/**
 * Created by koc9n on 4/16/16.
 */
'use strict';

var express = require('express');
var backend = require('./backend');
var auth = require('./api/auth');
var user = require('./api/user');
var coupon = require('./api/coupon');
var router = express.Router();

var env = process.env.NODE_ENV || "dev";
var config = require("../config/config.json")[env];

var API_PREFIX_URL = config.variables["api.prefix.url"] + config.variables["api.version"];

module.exports.init = function (app) {
    /**
     * handle all requests for testing
     */
    router.use(function (req, res, next) {
        if (env === "dev" || env === "test") {
            console.log("|| REQ QUERY ||\n " + JSON.stringify(req.query));
            console.log("|| REQ BODY ||\n " + JSON.stringify(req.query));
            console.log("|| REQ PATH VARIABLES ||\n " + JSON.stringify(req.query));
        }
        next();
    });

    /**
     * authentication filter
     */
    function isAuth (req, res, next) {
        var token = req.header('token');
        if (token === req.session.token) {
            next();
        } else {
            next(new Error("Auth Error"));
        }

    };

    /**
     * Define Api
     */
    router.use('/users', isAuth, user);
    router.use('/coupons', isAuth, coupon);
    router.use('/auth', auth);


    app.use(API_PREFIX_URL, router);

    app.use('/admin', backend);

};