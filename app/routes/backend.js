/**
 * Created by koc9n on 5/7/2016 AD.
 */
var express = require('express');
var router = express.Router();
var passport = require('passport');
var Strategy = require('passport-facebook').Strategy;

function convertFBProfileToUser(profile) {
    var user = {};
    user.id = profile.id;
    user.name = profile.displayName;
    user.email = profile.emails[0].value;
    user.photo = profile.photos[0].value;
    user.profileUrl = profile.profileUrl;
    return user;
}

function isAuthenticated(req, res, next) {
    if (req.user) {
        next();
    } else {
        var error = new Error("Permission denied");
        error.status = 403;
        next(error);
    }
}

/** passport FB strategy */
passport.use(new Strategy({
        clientID: process.env.FB_APP_ID,
        clientSecret: process.env.FB_APP_SECRET,
        callbackURL: process.env.FB_APP_CALLBACK_URL,
        scope: ['email'],
        profileFields: ['id', 'displayName', 'link', 'photos', 'email']
    },
    function (accessToken, refreshToken, profile, cb) {
        var user = convertFBProfileToUser(profile);
        var allowedUsers = !!process.env.ALLOWED_USERS ? process.env.ALLOWED_USERS.split(',') : '';
        var userIsAllowed = allowedUsers.indexOf(user.id) != -1;
        if (userIsAllowed || process.env.NODE_ENV != 'prod') {
            return cb(null, user);
        } else {
            var error = new Error("Permission denied");
            error.status = 403;
            return cb(error, null);
        }
    }));

passport.serializeUser(function (user, cb) {
    cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
    cb(null, obj);
});

router.use(passport.initialize());
router.use(passport.session());

/**
 * Define admin backend
 */

router.get("/", function (req, res) {
    res.render('login');
});

router.get("/profile", isAuthenticated, function (req, res) {
    res.render('index', {user: req.user});
});

router.get("/users", isAuthenticated, function (req, res) {
    res.render('userManager', {user: req.user});
});

router.get("/auth", passport.authenticate('facebook'));

router.get("/auth/callback",
    passport.authenticate('facebook', {failureRedirect: '/admin'}),
    function (req, res) {
        res.redirect('/admin/profile');
    });

router.get("/logout", isAuthenticated, function (req, res) {
    req.logOut();
    res.redirect('/admin');
});

module.exports = router;