/**
 * Created by koc9n on 4/9/16.
 */
var express = require('express');
var router = express.Router();
var userCtrl = require('../../controllers/UserCtrl');
var userDao = require('../../dao/sequelize/UserDAO');



/**
 * Router for Authentication
 * Working with User entity.
 */
/**
 * @apiDefine Header
 * @apiParam (Header parameters:) {String} token Access token in out system.
 */
/**
 * @apiDefine UserEntity success
 * @apiSuccess (Response: User) {STRING} id Id in our system.
 * @apiSuccess (Response: User) {STRING} facebookId Facebook Id associated with this user.
 * @apiSuccess (Response: User) {STRING} name User name.
 * @apiSuccess (Response: User) {STRING} lastName
 * @apiSuccess (Response: User) {INTEGER(1)} gender 0 - male, 1 - female
 * @apiSuccess (Response: User) {DATE} birthday format: YYYY MM DD
 * @apiSuccess (Response: User) {STRING} pictureUrl
 * @apiSuccess (Response: User) {BOOLEAN} online defaultValue: true
 * @apiSuccess (Response: User) {BOOLEAN} disableFAFOF defaultValue: false
 */

/**
 * @apiDefine ConfigEntity success
 * @apiSuccess (Success: Config) {INTEGER(3)} distance default 15
 * @apiSuccess (Success: Config) {INTEGER(2)} minAge default 18
 * @apiSuccess (Success: Config) {INTEGER(2)} maxAge default 81
 */
/**
 * @apiDefine VoteEntity success
 * @apiSuccess (Success: Vote) {INTEGER(1)} option choosed Option.
 * For male - [0 - NEXT, 1 - ANYTHING YOU WANT, 2 - MY OFFER].
 * For female - [0 - NO, 1 - YES]
 * @apiSuccess (Success: Vote) {INTEGER(1)} id facebookId of user from search
 */
router

/**
 * @api {get} /users 1. Get all Users
 * @apiVersion 0.0.1
 * @apiGroup User
 * @apiName Get all Users
 * @apiDescription
 * Retrieve array of all users
 * @apiUse Header
 * @apiUse UserEntity
 */
    .get('/', userCtrl.getAll)

    /**
     * @api {get} /users/me 2. Get me(authorised user)
     * @apiVersion 0.0.1
     * @apiGroup User
     * @apiName Get me(authorised user)
     * @apiDescription
     * Retrieve me(authorised user)
     * @apiUse Header
     * @apiUse UserEntity
     */
    .get('/me', userCtrl.getMe)

    /**
     * @api {get} /users/:id 3. Get user by id
     * @apiVersion 0.0.1
     * @apiGroup User
     * @apiName Get User by id
     * @apiDescription
     * Retrieve user by id
     * @apiUse Header
     * @apiParam (Path variable:) {String} :id path variable
     * @apiUse UserEntity
     */
    .get('/:id', userCtrl.get)

    /**
     * @api {post} /users/me 4. Update me(authorised user)
     * @apiVersion 0.0.1
     * @apiGroup User
     * @apiName Update me(authorised user)
     * @apiDescription
     * Update me(authorised user)
     * @apiUse Header
     * @apiUse UserEntity
     */
    .post('/me', userCtrl.update)

    /**
     * @api {delete} /users/me 5. Delete me(authorised user)
     * @apiVersion 0.0.1
     * @apiGroup User
     * @apiName Delete me(authorised user)
     * @apiDescription
     * Delete me(authorised user)
     * @apiUse Header
     * @apiUse UserEntity
     */
    .delete('/me', userCtrl.delete)

    /**
     * @api {post} /users/me/dating 6. Get users for dating (Milestone 2)
     * @apiVersion 0.0.1
     * @apiGroup User
     * @apiName Get Users for dating
     * @apiDescription
     * Get Users for dating
     * @apiUse Header
     * @apiUse UserEntity
     */
    .post('/me/dating', userCtrl.getUsersForDating)

    /**
     * @api {post} /users/me/vote  7. Make vote for user (Milestone 2)
     * @apiVersion 0.0.1
     * @apiGroup User
     * @apiName Make vote for user
     * @apiDescription
     * Make vote for user
     * @apiUse Header
     * @apiUse VoteEntity
     */
    .post('/me/vote', userCtrl.makeVote)

    /**
     * @api {get} /users/:token/config  8. Get config for user (Milestone 2)
     * @apiVersion 0.0.1
     * @apiGroup User
     * @apiName Get config for user
     * @apiDescription
     * Get config for user
     * @apiUse Header
     * @apiUse ConfigEntity
     */
    .get('/me/config', userCtrl.getConfig)

    /**
     * @api {put} /users/:token/config  9. Update config for user (Milestone 2)
     * @apiVersion 0.0.1
     * @apiGroup User
     * @apiName Update config for user
     * @apiDescription
     * Update config for user
     * @apiUse Header
     * @apiUse ConfigEntity
     */
    .put('/me/config', userCtrl.updateConfig);

module.exports = router;
