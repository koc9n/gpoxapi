/**
 * Created by koc9n on 4/9/16.
 */
var express = require('express');
var router = express.Router();
var authCtrl = require('../../controllers/AuthCtrl');


/**
 * @apiDefine UserAuthInfoEntity success 200
 * @apiSuccess (Response :) {STRING} token Token for our system.
 */
/**
 * @apiDefine AuthParam
 * @apiParam (Request parameters) {Number} facebookId Id from FB account.
 * @apiParam (Request parameters) {Number} facebookToken Token for FB api.
 */

/**
 * @api {post} /auth Authentication using POST method
 * @apiVersion 0.0.1
 * @apiGroup Auth
 * @apiName Auth Post
 * @apiDescription
 * Router for Authentication
 * register and login user from Facebook
 * available via POST method
 * @apiUse AuthParam
 * @apiUse UserAuthInfoEntity
 */

router.post("/", authCtrl.appAuth);

module.exports = router;