/**
 * Created by koc9n on 4/11/16.
 */
var express = require('express');
var router = express.Router();
var couponCtrl = require('../../controllers/CouponCtrl');

/**
 * Working with Coupon entity.
 */
/**
 * @apiDefine Header
 * @apiParam (Header parameters:) {String} token Access token in out system.
 */
/**
 * @apiDefine CouponEntity success
 * @apiSuccess (Response: Coupon) {STRING} id Id in our system.
 * @apiSuccess (Response: Coupon) {STRING} facebookId Facebook Id associated with this user.
 * @apiSuccess (Response: Coupon) {STRING} name User name.
 * @apiSuccess (Response: Coupon) {STRING} lastName
 * @apiSuccess (Response: Coupon) {INTEGER(1)} gender 0 - male, 1 - female
 * @apiSuccess (Response: Coupon) {DATE} birthday format: YYYY MM DD
 */
router
/**
 * @api {get} /coupons 1. Get all Coupons
 * @apiVersion 0.0.1
 * @apiGroup Coupon
 * @apiName Get all Coupons
 * @apiDescription
 * Retrieve array of all coupons
 * @apiUse Header
 * @apiUse CouponEntity
 */
    .get('/', couponCtrl.getCoupons)

    /**
     * @api {get} /coupons/:id 2. Get coupon by id
     * @apiVersion 0.0.1
     * @apiGroup Coupon
     * @apiName Get coupon by id
     * @apiDescription
     * Retrieve coupon by id
     * @apiUse Header
     * @apiParam (Path variable:) {String} :id path variable
     * @apiUse CouponEntity
     */
    .get('/:couponId', couponCtrl.getCoupon)

    /**
     * @api {get} /coupons 3. Delete coupon by id
     * @apiVersion 0.0.1
     * @apiGroup Coupon
     * @apiName Delete coupon by id
     * @apiDescription
     * Delete coupon by id
     * @apiUse Header
     * @apiUse CouponEntity
     */
    .delete('/:couponId', couponCtrl.deleteCoupon)

    /**
     * @api {get} /coupons 4. Add array of coupons
     * @apiVersion 0.0.1
     * @apiGroup Coupon
     * @apiName Add array of coupons
     * @apiDescription
     * Add array of coupons
     * @apiUse Header
     * @apiUse CouponEntity
     */
    .post('/list', couponCtrl.addCoupons)

    /**
     * @api {get} /coupons 5. Add coupon
     * @apiVersion 0.0.1
     * @apiGroup Coupon
     * @apiName Add coupon
     * @apiDescription
     * Add coupon
     * @apiUse Header
     * @apiUse CouponEntity
     */
    .post('/', couponCtrl.addCoupon);

module.exports = router;