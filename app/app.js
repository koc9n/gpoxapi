var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

/**
 * include routes
 * @type {router|exports|module.exports}
 */
var routers = require('./routes');

/**
 * include error handlers
 * @type {exports|module.exports}
 */
var errorHandler = require('./handlers/errorHandler');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/bower_components', express.static(path.join(__dirname, '../bower_components')));
app.use(require('express-session')({ secret: 'gpox secret 12345', resave: true, saveUninitialized: true }));


/**
 *  Routes for <gpox> Api
 */
routers.init(app);

// catch 404 and forward to error handler
app.use(errorHandler.handle404);
// error handlers
app.use(errorHandler.handleInternalError);


module.exports = app;