/**
 * Created by koc9n on 4/9/16.
 */
"use strict";
var userDao = require('../dao/sequelize/UserDAO');
var Promise = require("bluebird");
var fbService = Promise.promisifyAll(require('../services/FaceBookService'));
var userSrvc = require('../services/UserSrvc');
var geo = require('geolib');

module.exports.getAll = function (req, res, next) {
    userSrvc.getAll()
        .then(function (users) {
            return res.status(200).json(users);
        })
        .catch(function (err) {
            userSrvc.helper.catchError(err, next);
        });
};

module.exports.getMe = function (req, res, next) {
    var id = req.session.user;
    userSrvc.findById(id)
        .then(function (user) {
            return res.status(200).json(user);
        })
        .catch(function (err) {
            userSrvc.helper.catchError(err, next);
        });
};

module.exports.get = function (req, res, next) {
    var id = req.params.id;
    userSrvc.findById(id)
        .then(function (user) {
            return res.status(200).json(user);
        })
        .catch(function (err) {
            userSrvc.helper.catchError(err, next);
        });
};

module.exports.update = function (req, res, next) {
    var id = req.session.user;
    ;
    var userToUpdate = req.body;
    userSrvc.updateById(id, userToUpdate)
        .then(function (user) {
            return res.status(200).json(user);
        })
        .catch(function (err) {
            userSrvc.helper.catchError(err, next);
        });

};

module.exports.delete = function (req, res, next) {
    var id = req.session.user;
    userSrvc.delete(id)
        .then(function (user) {
            return res.status(200).json(user);
        })
        .catch(function (err) {
            userSrvc.helper.catchError(err, next);
        });
};

// TODO use one query to get and update config
module.exports.getConfig = function (req, res, next) {
    var id = req.session.user;
    userSrvc.getConfig(id)
        .then(function (config) {
            return res.status(200).json(config);
        })
        .catch(function (err) {
            userSrvc.helper.catchError(err, next);
        });
};

module.exports.updateConfig = function (req, res, next) {
    var config = req.body;
    var id = req.session.user;
    userSrvc.updateConfig(id, config)
        .then(function (config) {
            return res.status(200).json(config);
        })
        .catch(function (err) {
            userSrvc.helper.catchError(err, next);
        });
};

module.exports.getUsersForDating = function (req, res, next) {
    let id = req.session.user;
    let fbToken = req.session.fbToken;
    let me;
    userDao.findById(id)
        .then(function (user) {
            me = user;
            var excludedFromSearch = [];

            if (user.gender == null) {
                throw new Error("User should have gender before using searching.");
            } else if (user.gender === 0) {
                if (!user.Coupon) {
                    throw new Error("Male should have Coupon before using searching.");
                }
            }

            if (user.disableFAFOF && process.env.NODE_ENV === "prod") {
                excludedFromSearch = getFAFOF(user.facebookId, fbToken);
            }

            return Promise.resolve(excludedFromSearch)
        })
        .then(function (excludedFAFOF) {
            var excludedIds = [];
            excludedIds.concat(excludedFAFOF);
            excludedIds.concat(getExcludesFromRelations(me));

            var conditions = {};
            conditions.gender = me.gender === 0 ? 1 : 0;
            if (excludedIds.length > 0) {
                conditions.facebookId = {$notIn: excludedIds};
            }

            var scopes = [];
            scopes.push(me.gender === 0 ? 'femaleRelations' : 'maleRelations');
            scopes.push('includeCoupon');

            var attributes = [
                'facebookId',
                'name',
                'pictureUrl',
                'createdAt',
                'lat',
                'lng'
            ];

            return userDao.find(conditions, attributes, scopes);
        })
        .then(function (filteredUsers) {
            return res.status(200).json(findUsersAround(me, filteredUsers));
        })
        .catch(function (err) {
            userSrvc.helper.catchError(err, next);
        });

};

module.exports.makeVote = function (req, res, next) {
    var token = req.params.token;
    var vote = req.body;

    userSrvc.makeVote(token, vote)
        .then(function (status) {
            return res.status(200).json(status);
        })
        .catch(function (err) {
            userSrvc.helper.catchError(err, next);
        });
};

function getFAFOF(userFacebookId, userFacebookToken) {
    var _FAFOF_list = [];
    return fbService.getFriendsAsync(userFacebookId, userFacebookToken)
        .then(function (friends) {
            _FAFOF_list.push(friends.data);
            friends.data.forEach(function (friend, fIndex) {
                fbService.getFriendsAsync(friend.facebookId, userFacebookToken)
                    .then(function (friendsOfFriend) {
                        _FAFOF_list.push(friendsOfFriend.data);
                        if (friends.data.length - 1 == fIndex) {
                            return Promise.resolve(_FAFOF_list);
                        }
                    });
            });
        })

}

function getExcludesFromRelations(user) {
    var excluded = [];
    var userRelations = user.gender == 0 ? user.maleRelations : user.femaleRelations;
    userRelations.forEach(function (relation) {
        if (relation.maleChoise === 0 && relation.femaleChoise === 0) {
            excluded.push(user.gender == 0 ? relation.femaleUserId : relation.maleUserId)
        }
    });
    return excluded;
}

/**
 * Final filter by geolocation
 * @param filteredUsers
 * @returns {Array}
 */
//TODO its hack - should be changed to additional criteria in DB query. Find best way to create query using PostgreSql
function findUsersAround(user, filteredUsers) {
    var centerUser = user.get({
        plain: true
    });

    var filteredUsersAround = [];
    for (var i in filteredUsers) {
        var pointUser = filteredUsers[i].get({
            plain: true
        });

        if (pointUser.lat != null && pointUser.lng != null) {

            var isPointInCircle = geo.isPointInCircle({
                    latitude: parseFloat(pointUser.lat.toFixed(6)),
                    longitude: parseFloat(pointUser.lng.toFixed(6))
                },
                {latitude: centerUser.lat.toFixed(6), longitude: centerUser.lng.toFixed(6)},
                centerUser.Config.distance * 1000);

            if (isPointInCircle) {
                filteredUsersAround.push(pointUser);
            }
        }
    }
    return filteredUsersAround;
}
