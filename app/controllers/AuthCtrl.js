/**
 * Created by koc9n on 4/11/16.
 */
var userSrvc = require('../services/UserSrvc');

function getFBAuthData(req) {
    var fbUser = {};
    fbUser.facebookToken = req.body.facebookToken || req.query.facebookToken;
    fbUser.facebookId = req.body.facebookId || req.query.facebookId;
    return fbUser;
}

module.exports.appAuth = function (req, res, next) {
    var fbUser = getFBAuthData(req);

    userSrvc.authenticateFBUser(fbUser)
        .then(function (token) {
            req.session.token = token.token;
            req.session.user = token.user;
            req.session.fbToken = token.fbToken; // Save new facebook token in session
            return res.status(200).json({token: token.token});
        })
        .catch(function (err) {
            userSrvc.helper.catchError(err, next);
        });
};
