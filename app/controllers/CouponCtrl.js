/**
 * Created by koc9n on 4/11/16.
 */
var couponDao = require('../dao/sequelize/CouponDAO');

module.exports.getCoupons = function (req, res, next) {
    couponDao.findAll()
        .then(function (coupons) {
            return res.status(200).json(coupons);
        })
        .catch(function (err) {
            console.log(err);
            next(new Error("Sql: error: " + err.name));
        });
};

module.exports.getCoupon = function (req, res, next) {
    var couponId = req.params.couponId;
    couponDao.findByCouponId(couponId)
        .then(function (coupon) {
            return res.status(200).json(coupon);
        })
        .catch(function (err) {
            console.log(err);
            next(new Error("Sql: error: " + err.name));
        });
};

module.exports.deleteCoupon = function (req, res, next) {
    var couponId = req.params.couponId;
    couponDao.delete(couponId)
        .then(function (coupon) {
            return res.status(200).json(coupon);
        })
        .catch(function (err) {
            console.log(err);
            next(new Error("Sql: error: " + err.name));
        });
};

module.exports.addCoupons = function (req, res, next) {
    var coupons = req.body;
    couponDao.addCoupons(coupons)
        .then(function () {
            res.json({result: {success: "all coupons added"}});
        })
        .catch(function (err) {
            console.log(err);
            next(new Error("Sql: error: " + err.name));
        });
};

module.exports.addCoupon = function (req, res, next) {
    var coupon = req.body;
    console.log(coupon);

    couponDao.insert(coupon)
        .then(function (coupon) {
            res.json(coupon);
        })
        .catch(function (err) {
            console.log(err);
            next(new Error("Sql: error: " + err.name));
        });
};
